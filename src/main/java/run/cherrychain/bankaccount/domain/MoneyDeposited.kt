package run.cherrychain.bankaccount.domain

import run.cherrychain.bankaccount.domain.Event
import java.util.*

data class MoneyDeposited(
  override val id: UUID,
  val amount: Int
): Event
