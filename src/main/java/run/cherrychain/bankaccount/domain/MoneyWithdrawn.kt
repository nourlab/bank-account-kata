package run.cherrychain.bankaccount.domain

import run.cherrychain.bankaccount.domain.Event
import java.util.UUID

data class MoneyWithdrawn(override val id: UUID, val amount: Int, val fee: Int) : Event
