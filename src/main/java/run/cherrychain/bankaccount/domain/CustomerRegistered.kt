package run.cherrychain.bankaccount.domain

import java.util.*

data class CustomerRegistered(override val id: UUID, val fullName: String, val type: CustumerType) : Event
