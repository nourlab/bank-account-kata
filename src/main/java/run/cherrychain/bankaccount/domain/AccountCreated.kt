package run.cherrychain.bankaccount.domain

import run.cherrychain.bankaccount.domain.Event
import java.util.UUID

data class AccountCreated(override val id: UUID, val customerId: UUID) : Event
