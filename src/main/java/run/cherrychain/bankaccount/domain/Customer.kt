package run.cherrychain.bankaccount.domain

import java.util.*

class Customer {

  lateinit var type: CustumerType
  lateinit var id: UUID
  val changes = mutableListOf<Event>()

  fun register(id: UUID, fullName: String, type: CustumerType) {
    changes.add(CustomerRegistered(id, fullName, type))
  }

  fun hydrate(events: List<Event>) {
    events.forEach { event ->
      when (event) {
        is CustomerRegistered -> {
          this.id = event.id
          this.type = event.type
        }
      }
    }
  }

}

enum class CustumerType { Normal, Premium }
