package run.cherrychain.bankaccount.domain

import java.util.*

interface Event {
    val id: UUID
}

