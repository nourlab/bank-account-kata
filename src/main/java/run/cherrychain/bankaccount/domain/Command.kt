package run.cherrychain.bankaccount.domain

import java.util.*

sealed class Command

data class CreateAccount(val id: UUID, val customerId: UUID) : Command()
data class DepositMoney(val accountId: UUID, val amount: Int) : Command()
data class WithdrawMoney(val accountId: UUID, val amount: Int) : Command()
data class RegisterCustomer(val id: UUID, val fullName: String, val type: CustumerType): Command()
