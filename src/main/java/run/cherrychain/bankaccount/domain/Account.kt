package run.cherrychain.bankaccount.domain

import java.util.*

class Account() {
  lateinit var id: UUID
  lateinit var customerId: UUID
  var balance = 0;

  val change = mutableListOf<Event>()

  fun create(id: UUID, customerId: UUID) {
    change.add(AccountCreated(id, customerId))
  }

  fun deposit(amount: Int) {
    change.add(MoneyDeposited(id, amount))
  }

  fun withdraw(amount: Int, fee: Int) {
    change.add(MoneyWithdrawn(id, amount, fee))
  }

  fun hydrate(events: List<Event>) {
    events.forEach {event ->
      when (event) {
        is AccountCreated -> {
          this.id = event.id
          this.customerId = event.customerId
        }
        is MoneyDeposited -> this.balance += event.amount
        is MoneyWithdrawn -> this.balance -= event.amount
      }
    }
  }

  fun hasEnough(amount: Int): Boolean {
    return this.balance >= amount
  }

}
