package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.Event
import java.util.*

interface EventStore {
  fun append(event: Event)
  fun getEventsBy(id: UUID) : List<Event>
}

