package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.*
import run.cherrychain.bankaccount.port.view.EventHandler

class DefaultEventBus(private vararg val handlers: EventHandler) : EventBus {

  override fun emit(event: Event) {
      handlers.forEach { it.handle(event) }
  }
}
