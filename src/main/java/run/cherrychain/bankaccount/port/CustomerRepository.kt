package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.Customer
import java.util.*

class InMemoryCustomerRepository(private val eventStore: EventStore, private val eventBus: EventBus): Repository<Customer> {

  override fun put(customer: Customer) {
    customer.changes.forEach {
      eventStore.append(it)
      eventBus.emit(it)
    }
  }

  override fun get(id: UUID): Customer? {
    val customer = Customer()
    customer.hydrate(eventStore.getEventsBy(id))
    return customer
  }

}
