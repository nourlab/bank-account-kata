package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.*

interface CommandBus {
  fun publish(command: Command)
}
