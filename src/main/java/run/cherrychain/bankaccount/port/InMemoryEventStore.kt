package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.Event
import java.util.*

class InMemoryEventStore : EventStore {
  private val list = mutableListOf<Event>()

  override fun append(event: Event) {
    list.add(event)
  }

  override fun getEventsBy(id: UUID): List<Event> {
    return list.filter { it.id == id }
  }
}
