package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.*
import run.cherrychain.bankaccount.logic.CommandHandler

class DefaultCommandBus(private val handler: CommandHandler) : CommandBus {

  override fun publish(command: Command) {
    handler.handle(command)
  }

}
