package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.AccountCreated
import run.cherrychain.bankaccount.domain.CustomerRegistered
import run.cherrychain.bankaccount.domain.Event
import run.cherrychain.bankaccount.port.view.CustomerView
import run.cherrychain.bankaccount.port.view.CustomerViews
import run.cherrychain.bankaccount.port.view.EventHandler

class CustomerViewsEventHandler(private val customerViews: CustomerViews) : EventHandler {

  override fun handle(event: Event) {
    when(event) {
      is CustomerRegistered -> handle(event)
      is AccountCreated -> handle(event)
    }
  }

  fun handle(event: CustomerRegistered) {
    customerViews.add(CustomerView(event.id, event.fullName, listOf()))
  }

  fun handle(event: AccountCreated) {
    val view = customerViews.get(event.customerId)
    customerViews.add(
      view.copy(accountIds = view.accountIds + event.id)
    )
  }

}
