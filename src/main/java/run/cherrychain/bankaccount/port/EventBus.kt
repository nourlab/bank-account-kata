package run.cherrychain.bankaccount.port

import run.cherrychain.bankaccount.domain.Event

interface EventBus {
  fun emit(event: Event)
}
