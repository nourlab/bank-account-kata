package run.cherrychain.bankaccount.port.view

import java.util.*

data class CustomerView(val id: UUID, val fullName: String, val accountIds: List<UUID>)
