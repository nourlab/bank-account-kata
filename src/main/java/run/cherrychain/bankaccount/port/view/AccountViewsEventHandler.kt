package run.cherrychain.bankaccount.port.view

import run.cherrychain.bankaccount.domain.AccountCreated
import run.cherrychain.bankaccount.domain.Event
import run.cherrychain.bankaccount.domain.MoneyDeposited
import run.cherrychain.bankaccount.domain.MoneyWithdrawn

class AccountViewsEventHandler(private val accountViews: AccountViews) : EventHandler {
  override fun handle(event: Event) {
    when (event) {
      is AccountCreated -> handle(event)
      is MoneyDeposited -> handle(event)
      is MoneyWithdrawn -> handle(event)
    }
  }

  private fun handle(accountCreated: AccountCreated) {
    accountViews.insert(AccountView(
      id = accountCreated.id,
      customerId = accountCreated.customerId,
      balance = 0
    ))
  }

  private fun handle(moneyDeposited: MoneyDeposited) {
    val accountView = accountViews.get(moneyDeposited.id)
    val updatedView = accountView.copy(balance = accountView.balance + moneyDeposited.amount)
    accountViews.delete(moneyDeposited.id)
    accountViews.insert(updatedView)
  }

  private fun handle(moneyWithdrawn: MoneyWithdrawn) {
    val accountView = accountViews.get(moneyWithdrawn.id)
    val updatedView = accountView.copy(balance = accountView.balance - (moneyWithdrawn.amount + moneyWithdrawn.fee))
    accountViews.delete(moneyWithdrawn.id)
    accountViews.insert(updatedView)
  }
}
