package run.cherrychain.bankaccount.port.view

import run.cherrychain.bankaccount.port.view.AccountView
import java.util.*

interface AccountViews {
  fun insert(accountView: AccountView)
  fun get(id: UUID): AccountView
  fun delete(id: UUID): AccountView
}
