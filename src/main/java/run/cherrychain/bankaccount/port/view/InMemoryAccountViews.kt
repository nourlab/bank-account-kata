package run.cherrychain.bankaccount.port.view

import java.util.*

class InMemoryAccountViews : AccountViews {

  private val data = mutableMapOf<UUID, AccountView>()

  override fun insert(accountView: AccountView) {
    data.put(accountView.id, accountView)
  }

  override fun get(id: UUID): AccountView {
    return data.get(id)!!
  }

  override fun delete(id: UUID): AccountView {
    return data.remove(id)!!
  }

}
