package run.cherrychain.bankaccount.port.view

import java.util.*

data class AccountView(
  val id: UUID,
  val balance: Long,
  val customerId: UUID
)
