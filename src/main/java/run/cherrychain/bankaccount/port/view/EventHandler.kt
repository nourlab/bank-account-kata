package run.cherrychain.bankaccount.port.view

import run.cherrychain.bankaccount.domain.AccountCreated
import run.cherrychain.bankaccount.domain.Event
import run.cherrychain.bankaccount.domain.MoneyDeposited
import run.cherrychain.bankaccount.domain.MoneyWithdrawn

interface EventHandler {
  fun handle(event: Event)
}
