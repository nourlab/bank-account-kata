package run.cherrychain.bankaccount.port

import java.util.*

interface Repository<T> {
  fun put(entity: T)
  fun get(id: UUID): T?
}
