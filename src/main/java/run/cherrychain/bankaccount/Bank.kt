package run.cherrychain.bankaccount

import run.cherrychain.bankaccount.domain.*
import run.cherrychain.bankaccount.logic.CommandHandler
import run.cherrychain.bankaccount.logic.DefaultCommandHandler
import run.cherrychain.bankaccount.port.*
import run.cherrychain.bankaccount.port.view.*
import java.util.*

class Bank(
  private val accountViews: AccountViews = InMemoryAccountViews(),
  private val customerViews: CustomerViews = InMemoryCustomerViews(),
  private val eventBus: EventBus = DefaultEventBus(AccountViewsEventHandler(accountViews), CustomerViewsEventHandler(customerViews)),
  private val eventStore: EventStore = InMemoryEventStore(),
  private val handler: CommandHandler = DefaultCommandHandler(InMemoryAccountRepository(eventStore, eventBus), InMemoryCustomerRepository(eventStore, eventBus)),
  private val commandBus: CommandBus = DefaultCommandBus(handler = handler)
) {

  fun createAccount(customerId: UUID): UUID {
    val id = UUID.randomUUID()
    commandBus.publish(CreateAccount(id, customerId));
    return id;
  }

  fun deposit(accountId: UUID, amount: Int) {
    commandBus.publish(DepositMoney(accountId, amount))
  }

  fun withdraw(accountId: UUID, amount: Int) {
    commandBus.publish(command = WithdrawMoney(accountId, amount))
  }

  fun getAccount(accountId: UUID): AccountView {
    return accountViews.get(accountId)
  }

  fun registerCustomer(fullName: String, type: CustumerType): UUID {
    val id = UUID.randomUUID()
    commandBus.publish(RegisterCustomer(id, fullName, type))
    return id
  }

  fun getCustomer(customerId: UUID): CustomerView {
    return customerViews.get(customerId)
  }

}
