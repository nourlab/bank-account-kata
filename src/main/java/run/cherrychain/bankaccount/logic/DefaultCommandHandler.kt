package run.cherrychain.bankaccount.logic

import run.cherrychain.bankaccount.BankException
import run.cherrychain.bankaccount.port.Repository
import run.cherrychain.bankaccount.domain.*

class DefaultCommandHandler(private val accountRepository: Repository<Account>, private val customerRepository: Repository<Customer>) : CommandHandler {

  override fun handle(command: Command) {
    when (command) {
      is CreateAccount -> create(command)
      is DepositMoney -> deposit(command)
      is WithdrawMoney -> withdraw(command)
      is RegisterCustomer -> registerCustomer(command)
    }
  }

  private fun registerCustomer(command: RegisterCustomer) {
    val customer = Customer()
    customer.register(command.id, command.fullName, command.type)
    customerRepository.put(customer)
  }

  private fun create(createAccount: CreateAccount) {
    val currentAccount = Account()
    currentAccount.create(createAccount.id, createAccount.customerId)
    accountRepository.put(currentAccount)
  }

  private fun deposit(depositMoney: DepositMoney) {
    val currentAccount = accountRepository.get(id = depositMoney.accountId)

    if (currentAccount!=null) {
      currentAccount.deposit(depositMoney.amount)
      accountRepository.put(currentAccount)
    } else {
      throw BankException("account does not exist")
    }
  }

  private fun withdraw(withdrawMoney: WithdrawMoney) {
    val currentAccount = accountRepository.get(id = withdrawMoney.accountId)
    val currentCustomer = customerRepository.get(currentAccount!!.customerId)
    val fee = when (currentCustomer!!.type) {
        CustumerType.Normal -> 2
        else -> 0
    }

    if (currentAccount.hasEnough(withdrawMoney.amount + fee)) {
      currentAccount.withdraw(withdrawMoney.amount, fee)
      accountRepository.put(currentAccount)
    } else {
      throw BankException("not enough money")
    }


  }
}
