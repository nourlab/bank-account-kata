package run.cherrychain.bankaccount.logic

import run.cherrychain.bankaccount.domain.Command

interface CommandHandler {

  fun handle(command: Command)
}
