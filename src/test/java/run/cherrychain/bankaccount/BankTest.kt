package run.cherrychain.bankaccount

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import run.cherrychain.bankaccount.domain.CustumerType.*
import java.util.UUID.randomUUID

class BankTest {
  private val bank = Bank()

  @Test
  internal fun `it should create an account`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)
    val accountId = bank.createAccount(customerId)

    val account = bank.getAccount(accountId)
    assertThat(account.id).isEqualTo(accountId)
    assertThat(account.balance).isEqualTo(0)
    assertThat(account.customerId).isEqualTo(customerId)
  }

  @Test
  internal fun `customer should have a list of accounts`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)
    val firstAccountId = bank.createAccount(customerId)
    val secondAccountId = bank.createAccount(customerId)

    val customer = bank.getCustomer(customerId)
    assertThat(customer.accountIds).containsExactlyInAnyOrder(firstAccountId, secondAccountId)
  }

  @Test
  internal fun `it should deposit in an account`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)
    val accountId = bank.createAccount(customerId)

    bank.deposit(accountId, 100)

    val account = bank.getAccount(accountId)
    assertThat(account.balance).isEqualTo(100)
  }

  @Test
  internal fun `it should not deposit if account does not exist`() {
    assertThrows<BankException> {
      bank.deposit(randomUUID(), 1)
    }
  }

  @Test
  internal fun `it should withdraw from an account with fee`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)
    val accountId = bank.createAccount(customerId)

    bank.deposit(accountId, 150)
    bank.withdraw(accountId, 50)

    val account = bank.getAccount(accountId)
    assertThat(account.balance).isEqualTo(98)
  }

  @Test
  internal fun `it should withdraw from an account of a premium customer without fee`() {
    val customerId = bank.registerCustomer("Rich Person", Premium)
    val accountId = bank.createAccount(customerId)

    bank.deposit(accountId, 150)
    bank.withdraw(accountId, 50)

    val account = bank.getAccount(accountId)
    assertThat(account.balance).isEqualTo(100)
  }

  @Test
  internal fun `it should not withdraw from an account when poor`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)
    val accountId = bank.createAccount(customerId)

    bank.deposit(accountId, 150)

    assertThrows<BankException> {
      bank.withdraw(accountId, 150)
    }
  }

  @Test
  internal fun `it should register a customer`() {
    val customerId = bank.registerCustomer("Nome Cognome", Normal)

    assertThat(bank.getCustomer(customerId).fullName).isEqualTo("Nome Cognome")
  }

}
